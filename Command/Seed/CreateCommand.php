<?php

namespace Alexssssss\PhinxBundle\Command\Seed;

use Alexssssss\PhinxBundle\Command\CommonTrait;

class CreateCommand extends \Phinx\Console\Command\SeedCreate
{
    use CommonTrait;

    protected static $defaultName = "phinx:seed:create";
}