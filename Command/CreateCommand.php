<?php

namespace Alexssssss\PhinxBundle\Command;

use Alexssssss\PhinxBundle\Command\CommonTrait;

class CreateCommand extends \Phinx\Console\Command\Create
{
    use CommonTrait;

    protected static $defaultName = "phinx:create";
}